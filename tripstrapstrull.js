$(document).ready(function(){
$("#voitKuvame").hide();
$(".mang").on("click tap",function(){
	//nüüd vaatame käiku, kus marker on kujul 'XO'- X on inimene, aga O arvuti.
  $("#vqrk").attr("markerid",'XO');
  $("#esimeneAken").hide();
  $("#esimeneAken").empty();
});


//kontrollime kas käiku üldse võib teha või mitte, tagastab TRUE või FALSE 
function legaalneKaik(kaik,laud){
  return laud[kaik.findIndex((elem) => elem !== 0)] < 1 ;
}


//kontrollime kes on mängu võitja. Ta võttab argumendiks praeguse tahvli seisundit ning mängijat
//tagastab True kui see mängija on võitja.
function kesVoidab(laud,mark){

  //List kombinatsioonidest, mis on vajalikud võiduks
  var voiduKomb = [[1,1,1,0,0,0,0,0,0],
                 [0,0,0,1,1,1,0,0,0],
                 [0,0,0,0,0,0,1,1,1],
                 [1,0,0,1,0,0,1,0,0],
                 [0,1,0,0,1,0,0,1,0],
                 [0,0,1,0,0,1,0,0,1],
                 [1,0,0,0,1,0,0,0,1],
                 [0,0,1,0,1,0,1,0,0]];
				 
   //kustutame mängijat mis ei sobi, kui ta ei vasta laua seisundile
   laud = laud.map((elem) => +(elem===mark));


  
   //kas mingi meie kombinatsioonidest on voiduKomb sees. Vaatame iga meie võidu kombinatsiooni.  Some() funktsioon tagastab TRUE kui on mingi sees.
  var kasVoitja = voiduKomb.some(function(voitKombinatsioon){
  //kui 0 mitte panna, siis ei ole õige võitja- peame võtta ainult X või O (mis on esimene) , aga mitte koos
    return voitKombinatsioon.reduce(((sum, elem, i) => sum + (elem && laud[i])),0) == 3;
  });
  return kasVoitja;
}


//funktsioon selleks, et saada meie tahvli kujul a0, a1,a2,a3,a4,a5,a6,a7,a8(nagu HTML failis on selgitatud)
function getLaudSeis(){
  var laud = [];
  //Teeme tsükli millega läbime kõik meie laua positsioonid lisades sellele väärtuse mis vastab positsioonile
  //kui aga väärtust ei ole siis 0
  for (i=0; i < 9; i++){
    laud.push($("#a"+i).text().trim() ? $("#a"+i).text().trim() : 0);
  }
  return laud;
}


//https://medium.freecodecamp.org/how-to-make-your-tic-tac-toe-game-unbeatable-by-using-the-minimax-algorithm-9d690bad4b37
//https://tproger.ru/translations/tic-tac-toe-minimax/


//selles funktsioonis me teeme arvutikäike ja kutsume funktsiooni mis kasutab minmax algoritmi.
function AIkaik(laud,AImark){
  
  //parimad käigud
  var parimKaigud=0;
  //alguses on väärtused 0 igalpool
  var koht = new Array(9).fill(0);

  //käigud mida me saame teha
  var kaigud = laud.reduce(function(retarray,elem,n){
    if (elem ==0){
      retarray.push(n);
    }
    return retarray;
  },[]);

    // läbime kõige perimaid käike ning salvestame minimax väärtust skoor väärtuse sisse
    parimKaigud = kaigud.reduce(function(skoor,elem){
      //teeme uue laua ja lisame sinna käigu ja skoori (kui hea see käik on)
      var uuslaud= laud.map(elem => elem);
      uuslaud[elem]=AImark;
      skoor.push(arvutameAIkaik(uuslaud,'XO'[+!'XO'.indexOf(AImark)]));
      return skoor;
    },[]);
	//tagastame numbri mis on parima käigu kasti oma. 1 kast on 0, 2 kast on 1 jne. Ta on indeksiga 2. 0 indeksiga on 0, 1 indeksiga on -1
    koht[kaigud[parimKaigud.indexOf(Math.max.apply(null,parimKaigud))]]=2;
  
  return koht.indexOf(2);
}

//selle funktsiooniga me arutame mängu seisu minimax algoritmi jaoks
//tagastame 10 kui AI võidab ja -10 kui inimene ning 0 kui viik
//kui aga mäng veel ei ole lõppenud siis tagastame mäng on jätkamas

//tulemus- käikude skoor
function tulemus(laud,mark){
  var maarkerid = $("#vqrk").attr("markerid");
  if (kesVoidab(laud,mark)){
    return mark == 'O' ? 10 : -10;
  }
  else if(laud.indexOf(0)<0){
    return 0;
  }
  return "jätkamas";
}

//kui kasutaja klikkib mingi laua kastikese peale siis kasutame funktsiooni:
$(".vqrguAsukoht").on("click tap",function(){
    var marker = $("#vqrk").attr("markerid");
    var laud = getLaudSeis();
    var kaik = new Array(9).fill(0);


	//inimene tegi käiku ning nüüd teeme uut listi kus on numbri asemel kasutaja marker ehk 'X'
    kaik[$(this).attr("id").replace(/\D+/,"")]='X';

	//selleks et ei oleks tehtav käik mida ei saa teha ja pärast mängu lõppetamist keegi ei saaks muuta mängu tulemust loome tingimust
    if (!legaalneKaik(kaik,laud) || kesVoidab(laud,'X') || kesVoidab(laud,'O')){
      return;
    }
	// kasutaja klikkis, nüüd on vaja kuvama tema markerit ehk 'X'
    $(this).text('X'); 
	//nüüd on uus laua seis. Lisame lauale 'X'
    laud = getLaudSeis('X'); 

	//kontrollime kas inimene on võitnud, kutsume funktsiooni m2nguL6pp()
    if (kesVoidab(laud,'X')){
      m2nguL6pp('X');
      return;
    }

	//kui inimene ei ole võitnud siis anname AI võimalust käia, selleks kasutame AIkaik funktsiooni, selleks et arvuti teeks käigu ja pärast kuvame 
	//AI markerit laua peal, ehk 'O'. Ja nagu üleval uuendame laua seisundit.
    
    $("#a"+AIkaik(laud,'O')).text('O'); 
    laud = getLaudSeis(); 
	
	
    //kontrollime kas AI on võitnud, kutsume funktsiooni m2nguL6pp()
    if (kesVoidab(laud,'O')){
      m2nguL6pp('O');
      return;
    }

	//Viiki kontrollimine, kutsume funktsiooni m2nguL6pp(). Kui meil ei ole tühjasid kastikesi:
    if (laud.indexOf(0)<0){
      m2nguL6pp();
    }
});


//kasutame minimax algooritmi kasutades rekursiooni selleks et leida paremat käiku
//rekursiivselr läbime kõik võimalikud käigud

//võimalikud käigud - kuhuMinna
function arvutameAIkaik(laudSeis,praeguMark){
  var maarkerid = $("#vqrk").attr("markerid");
  var eelmineMark = maarkerid[+!maarkerid.indexOf(praeguMark)];
  var parim = 0;
  var skoorid=[];
  var kuhuMinna=[];

  //Vaatame skooride käike(tulemust) selleks et kontrollida rekursiivselt kas mäng on jätkamas

  if(tulemus(laudSeis,eelmineMark)!="jätkamas"){
    return tulemus(laudSeis,eelmineMark);
  }
  //kuhu minna- paneme kõik võimalikud käigud selle muutuja sisse
  //nt  [2, 7, 8]
  var kuhuMinna = laudSeis.reduce(function(listike,elem,n){
    if (elem == 0){ listike.push(n); }
    return listike;
	},[]);

  //vaatame tsükliga kõik võimalikud käigud ja tagastame skoori iga käigu jaoks
  kuhuMinna.forEach(function(elem){
      var uuslaud= laudSeis.map(elem => elem);
      uuslaud[elem]=praeguMark;

	    //nt [0,-10]
		//nt  [-10, 10, 0]
      skoorid.push(arvutameAIkaik(uuslaud,'XO'[+!'XO'.indexOf(praeguMark)]));
	
  });
  //leiame maksimaalset või minimaalset väärtust selleks et leida kõige parimat käigu
  //max väärtust me otsime kui on AI kord, min väärtust kui käib inimene
  if (praeguMark == 'O'){
    parim = -1000000;
    skoorid.push(parim);
    return Math.max.apply(null,skoorid);
	
  }
  else {
    parim = 100000;
    skoorid.push(parim);
	
    return Math.min.apply(null,skoorid);

  }
}



function m2nguL6pp(kesVoitis){
  // kui mäng on lõppenud siis kuvame infot selle kohta mis on mängu kokkuvõtte (kes võitis) ja kustutame kõik lauast maha selleks et alustada uut mängu

  var marker = $("#vqrk").attr("markerid");
  $("#voitKuvame").show();
  $("#voitTekst").text(arguments.length > 0 ? kesVoitis + " oli praegu edukam": "Tuli viik");

  //Puhastame välja kui kasutaja klikkib nuupu peale: Mängi veel
  $("#uusMang").on("click tap",function(){
    for (i = 0; i < 9; i++){
      $("#a"+i).empty();
    }
	//peidame võidu tulemust
    $("#voitKuvame").hide();


  });
}


});
