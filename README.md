# Trips Traps Trull


Mängu koostamisel kasutasin *HTML*, *CSS*, *JS* ning *Bootstrap* elemente. 


Selleks, et käivitada mängu on vaja alla laadida seda (kloonida projekti) oma arvutile, ning pärast käivitada HTML- faili. 


Esimese käigu teeb alati inimene ('X'), teise arvuti ('O') jne. Arvuti kasutab minimax algoritmi selleks, et arvutada parimat käiku. Sellest algoritmist meile räägiti ülikoolis, ning samuti ma lugesin rohkem Internetist:

* https://medium.freecodecamp.org/how-to-make-your-tic-tac-toe-game-unbeatable-by-using-the-minimax-algorithm-9d690bad4b37

* https://tproger.ru/translations/tic-tac-toe-minimax/

Arvuti kunagi ei kaota, teeb nii, et mängu tulemuseks oleks viik. 


